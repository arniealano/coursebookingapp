let parameter = window.location.search;
let token = localStorage.getItem('token');

const urlId = new URLSearchParams(parameter);
let id = urlId.get('id');


if(id){
    if(!token){
        alert("You are not currently logged in. Please sign in first to proceed");
        window.location.replace('./login.html');
    }else{
        fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}`, {
            method: 'GET'
        }).then(res => {
            return res.json()
        }).then(data => {           
            let html = `<h2 class="text-center my-5 form-header">Edit A Course</h2>
            <form action="" id="editCourse">
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <input type="text" id="courseName" class="form-control border-0" value="${data.name}">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="number" id="coursePrice" class="form-control border-0" value="${data.price}">
                    </div>
                </div>
                <input type="text" id="courseDescription" class="form-control border-0" value="${data.description}">
                <a href="./editCourse.html?id=${data._id}" class="btn btn-block custom-button text-white" id="update-course">
                Submit Updates</a>
            </form>`;
            document.querySelector('#form-template').innerHTML = html; 
            document.querySelector('#update-course').addEventListener('click', e => {
                e.preventDefault();
                
                let name = document.querySelector('#courseName').value;
                let price = document.querySelector('#coursePrice').value;
                let description = document.querySelector('#courseDescription').value; 

                fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}/update-course`, {
                    method: 'PATCH',
                    headers: {
                      'Content-Type' : 'application/json'
                            },
                    body: JSON.stringify({
                        name: name,
                        price: price,
                        description: description
                    })                    
                }).then( res => {
                    return res.json()
                }).then(data => {
                    if(data === true){
                        alert('Updated successfully');
                    }else{
                        alert("Something went wrong");
                    }
                })
            })
        });
    }
}