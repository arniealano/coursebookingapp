//Initialize page
//Get the token to know the identity of the user who logged-in
let token = localStorage.getItem('token');
let profile = document.querySelector('#profile-template');

fetch('https://polar-citadel-90746.herokuapp.com/api/users/details', {
    method: 'GET',
    headers: {
        authorization: `Bearer ${token}`
    }
}).then(res => {
    return res.json()
}).then(data => {
    let table;
    profile.innerHTML = 
    `<div class="px-4">
    <div class="form-group row">
        <label for="firstName" class="col-sm-2 col-form-label text-dark">First Name: </label>
        <div class="col-sm-10">
          <input type="text" readonly class="form-control" id="firstName" value="${data.firstName}">
        </div>
    </div>
    <div class="form-group row">
        <label for="lastName" class="col-sm-2 col-form-label text-dark">Last Name: </label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control" id="lastName" value="${data.lastName}">
        </div>
    </div>
    <div class="form-group row">
        <label for="mobileNumber" class="col-sm-2 col-form-label text-dark">Mobile Number: </label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control" id="mobileNumber" value="${data.mobileNo}">
        </div>
    </div>					
    </div>`;

for(let i = 0; i<data.enrollments.length; i++){
    if(table){
        table =
        `${table}
        <tr>
            <th scope="row">${i + 1}</th>
            <td>${data.enrollments[i].name}</td>
            <td>${data.enrollments[i].enrolledOn}</td>
            <td>${data.enrollments[i].status}</td>
        </tr>`;
    }else{
        table =
        `<tr>
            <th scope="row">${i + 1}</th>
            <td>${data.enrollments[i].name}</td>
            <td>${data.enrollments[i].enrolledOn}</td>
            <td>${data.enrollments[i].status}</td>
        </tr>`;
    } 
}
if(table){
    table = 
    `<table class="table table-hover table-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>Course Name</th>
            <th>Enrolled On</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>${table}</tbody>
    </table>`;
}
if(table){
    document.querySelector('#table-template').innerHTML = table;
}   
});