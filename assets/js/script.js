//Initialize Nav
let tokenVar = localStorage.getItem('token');
let isAdminVar = localStorage.getItem('isAdmin');

let path = window.location.pathname;
let page = path.split('/').pop();
let index_nav;
let courses_nav;
let profile_nav;
let logout_nav;
let register_nav;
let login_nav;
let indexdir;
let dirname;

switch (page) {
    case 'index.html':
        index_nav = 'active';
        break;
    case 'courses.html':
        courses_nav = 'active';
        break;
    case 'profile.html':
        profile_nav = 'active';
        break;    
    case 'logout.html':
        logout_nav = 'active';
        break;   
    case 'register.html':
        register_nav = 'active';
        break;   
    case 'login.html':
        login_nav = 'active';
        break;                                       
    default:
        break;
}

if(page === 'index.html'){
    indexdir = './';
    dirname = './pages/';
}else{
    indexdir = '../';
    dirname = './';
}

//Checked if signed in
if(tokenVar){
    if(isAdminVar === "true"){
        document.querySelector('#zuitterNav').innerHTML = 
        `<ul class="navbar-nav ml-auto">
            <li class="nav-item ${index_nav}">
                <a href="${indexdir}index.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item ${courses_nav}">
                <a href="${dirname}courses.html" class="nav-link">Courses</a>
            </li>        			
            <li class="nav-item ${logout_nav}">
                <a href="${dirname}logout.html" class="nav-link">Sign Out</a>
            </li>							
        </ul>`;
    }else{
        document.querySelector('#zuitterNav').innerHTML = 
        `<ul class="navbar-nav ml-auto">
            <li class="nav-item ${index_nav}">
                <a href="${indexdir}index.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item ${courses_nav}">
                <a href="${dirname}courses.html" class="nav-link">Courses</a>
            </li>        
            <li class="nav-item ${profile_nav}">
                <a href="${dirname}profile.html" class="nav-link">Profile</a>
            </li>			
            <li class="nav-item ${logout_nav}">
                <a href="${dirname}logout.html" class="nav-link">Sign Out</a>
            </li>							
        </ul>`;
    }
}else{
    document.querySelector('#zuitterNav').innerHTML = 
    `<ul class="navbar-nav ml-auto">
        <li class="nav-item ${index_nav}">
            <a href="${indexdir}index.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item ${courses_nav}">
            <a href="${dirname}courses.html" class="nav-link">Courses</a>
        </li>        
        <li class="nav-item ${register_nav}">
            <a href="${dirname}register.html" class="nav-link">Register</a>
        </li>			
        <li class="nav-item ${login_nav}">
            <a href="${dirname}login.html" class="nav-link">Sign In</a>
        </li>							
    </ul>`;    
}