let isAdmin =  localStorage.getItem('isAdmin');

//Only display create course button if the user has an admin role.
if(isAdmin === "true") {
    document.querySelector('#create-access').innerHTML = `<a href="../pages/addCourse.html" class="btn animate-button" id="create-course-button">Create Course</a>`
}

fetch('https://polar-citadel-90746.herokuapp.com/api/courses', {
    method: 'GET'
}).then(res => {
    return res.json()
}).then(data => {
    let html;
    let button;
    if(isAdmin === "false"){
        button = 'Book Now';
    }else{
        button = 'Display Course';
    }
    for(let i = 0; i< data.length; i++){
        if(isAdmin === "false" && data[i].isActive === false) continue;
        if(html){
            html = `${html}<div class="card ml-2 courses-card">
            <h5 class="card-header custom-card-header">${data[i].name}</h5>
            <div class="card-body">
                <p>${data[i].description}</p>
                <div class="float-right font-weight-bold pb-2">
                    <span>For Only </span><span id="price-1">$ ${data[i].price}</span>
                </div>														
            </div>		
            <div class="card-footer">
                <a href="./course.html?id=${data[i]._id}" class="btn btn-block custom-button text-white onclick">
                    ${button}
                </a>	
            </div>					
        </div>`;
        }else{
            html = `<div class="card ml-2 courses-card">
            <h5 class="card-header custom-card-header">${data[i].name}</h5>
            <div class="card-body">
                <p>${data[i].description}</p>
                <div class="float-right font-weight-bold pb-2">
                    <span>For Only </span><span id="price-1">$ ${data[i].price}</span>
                </div>														
            </div>		
            <div class="card-footer">
                <a href="./course.html?id=${data[i]._id}" class="btn btn-block custom-button text-white onclick">
                    ${button}
                </a>	
            </div>					
        </div>`;
        }
    }
    if(html){
        document.querySelector('#courseContainer').innerHTML = html;
    }
})






