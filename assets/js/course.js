let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');

let parameter = window.location.search;
const urlId = new URLSearchParams(parameter);
let id = urlId.get('id');

const getHtmlTemplate = (data, isAdministrator, readOnly) => {

    if(isAdministrator === "true"){
        return `<h2 class="text-center my-5 form-header">Course Details</h2>
        <form id="createCourse">
            <div class="form-row">
                <div class="form-group col-md-9">
                    <input type="text" class="form-control border-0" id="courseName" value="${data.name}" ${readOnly}>
                </div>
                <div class="form-group col-md-3">
                    <input type="number" id="coursePrice" class="form-control border-0" value="${data.price}" ${readOnly}>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="text" id="courseDescription" class="form-control border-0" value="${data.description}" ${readOnly}>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <a href="./editCourse.html?id=${data._id}" class="btn btn-block custom-button text-white" id="edit-course">
                    Edit Course</a>
                </div>
                <div class="form-group col-md-4">
                    <a href="./course.html?id=${data._id}" class="btn btn-block custom-button text-white" id="delete-course">
                    Delete Course</a>
                </div>		
                <div class="form-group col-md-4">
                    <a href="./course.html?id=${data._id}" class="btn btn-block custom-button text-white" id="reactivate-course">
                    Reactivate Course</a>
                </div>												
            </div>					
            <div id="message">						
        </div>`;
    }else{
        return `<h2 class="text-center my-5 form-header">Course Details</h2>
        <form id="createCourse">
            <div class="form-row">
                <div class="form-group col-md-9">
                    <input type="text" class="form-control border-0" id="courseName" value="${data.name}" ${readOnly}>
                </div>
                <div class="form-group col-md-3">
                    <input type="number" id="coursePrice" class="form-control border-0" value="${data.price}" ${readOnly}>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <input type="text" id="courseDescription" class="form-control border-0" value="${data.description}" ${readOnly}>
                    <a href="./course.html?id=${data._id}" class="btn btn-block custom-button text-white" id="book-now-submit">
                    Book Now</a>
                </div>
            </div>
            <div id="message">						
            </div>				
        </form>`;
    }

}

if(id){
    if(!token){
        alert("You are not currently logged in. Please sign in first to proceed");
        window.location.replace('./login.html');
    }else{
        fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}`, {
            method: 'GET'
        }).then(res => {
            return res.json()
        }).then(data => {
            let readOnly = 'readonly';
            let table;
            let html = getHtmlTemplate(data, isAdmin, readOnly);
            //This is a separete logic to display enrolled users as table.
            if(isAdmin === "true"){
                for(let i = 0; i<data.enrollees.length; i++){
                    if(table){
                        table =
                        `${table}<tr>
                            <th scope="row">${i + 1}</th>
                            <td>${data.enrollees[i].firstName}</td>
                            <td>${data.enrollees[i].lastName}</td>
                            <td>${data.enrollees[i].enrolledOn}</td>
                            <td>${data.enrollees[i].status}</td>
                        </tr>`;
                    }else{
                        table =
                        `<tr>
                            <th scope="row">${i + 1}</th>
                            <td>${data.enrollees[i].firstName}</td>
                            <td>${data.enrollees[i].lastName}</td>
                            <td>${data.enrollees[i].enrolledOn}</td>
                            <td>${data.enrollees[i].status}</td>
                        </tr>`;
                    }
                }
                if(table){
                    table = 
                    `<table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Enrolled On</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>${table}</tbody>
                    </table>`;
                }
            }
           
            document.querySelector('#form-template').innerHTML = html;  
            if(table){
                document.querySelector('#enrollees-table').innerHTML = table;
            }
            
            if(data.isActive === false) {
                document.querySelector('#status').innerHTML = `<span class="h3 text-dark">Status:</span>
                <span class="h3 text-danger">
                  Inactive
                </span>`;
            }
            //update user's enrollments list
            if(isAdmin === "false"){
                document.querySelector('#book-now-submit').addEventListener('click', e => {
                    e.preventDefault();
                    let name = document.querySelector('#courseName').value;
   
                   fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}/check-enrollment`, {
                       method: 'GET',
                       headers: {
                                   authorization: `Bearer ${token}`
                               }
                   }).then(res => {
                       return res.json()
                   }).then(data => {
                       console.log(data);
                       //Users cannot enroll multiple times in the same course.
                       if(data === false){
                           fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}`,{
                               method: 'PATCH',
                               headers: {
                                   authorization: `Bearer ${token}`,
                                   'Content-Type' : 'application/json'
                               },
                               body: JSON.stringify({
                                name: name
                            })                                
                           }).then(res => {
                               return res.json()
                           }).then(data => {
                               if(data === true){
                                   alert('You have successfully enrolled to this training.');
                               }else{
                                   alert('Something went wrong');
                               }
                           })
                       }else{
                           alert('You cannot enroll multiple times in the same course!');
                       }
                   })
   
                })
            }
        if(isAdmin === "true"){
            document.querySelector('#delete-course').addEventListener('click', e => {
                e.preventDefault();
                fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}/deactivate-course`, {
                    method: 'PATCH'                   
                }).then(res => {
                    return res.json()
                }).then(data => {
                    if(data === true) {
                        document.querySelector('#status').innerHTML = `<span class="h3 text-dark">Status:</span>
                        <span class="h3 text-danger">
                          Inactive
                        </span>`;
                        alert('Course has been deactivated');
                    }else{
                        alert('Something went wrong while trying to deactivate course. Please retry');
                    }
                })
            })

            document.querySelector('#reactivate-course').addEventListener('click', e => {
                e.preventDefault();
                fetch(`https://polar-citadel-90746.herokuapp.com/api/courses/${id}/activate-course`, {
                    method: 'PATCH'                   
                }).then(res => {
                    return res.json()
                }).then(data => {
                    if(data === true) {
                        document.querySelector('#status').innerHTML = '';
                        alert('Course has been reactivated');
                    }else{
                        alert('Something went wrong while trying to deactivate course. Please retry');
                    }
                })
            })            
        }
        });
    }
}

