let form = document.querySelector('#createCourse');

form.addEventListener('submit', e => {
    e.preventDefault();
    // token = localStorage.getItem('token');
    let name = document.querySelector('#courseName').value;
    let description = document.querySelector('#courseDescription').value;
    let price = document.querySelector('#coursePrice').value;   
    if(name && description && price) {          
        fetch('https://polar-citadel-90746.herokuapp.com/api/courses', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })            
        }).then( res => {
            return res.json()
        }).then(data => {
            console.log(data);
            if(data === true) {
                //Clear input fields and return success message.
                document.querySelector('#courseName').value = '';
                document.querySelector('#courseDescription').value = '';
                document.querySelector('#coursePrice').value = '';  
                document.querySelector('#message').innerHTML = `<div class="alert alert-success" role="alert">Course has been created successfully!</div>`;                 
            }else{
                document.querySelector('#message').innerHTML = `<div class="alert alert-danger" role="alert">Something went wrong. Please check your data.</div>`;
            }
        })
    }else{
        alert('Course Name, Description and Price are all required.')
    }
    
})