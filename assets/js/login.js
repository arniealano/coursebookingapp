let loginForm = document.querySelector('#loginUser');

loginForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let email = document.querySelector("#userEmail").value;
    let password = document.querySelector("#password").value;

    if(!email || !password){
        alert('Please enter your email address and password');
    }else{
        fetch('https://polar-citadel-90746.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'content-type' : 'application/json'
            },
            body: JSON.stringify(
                {
                    email: email,
                    password: password
                }
            )
        }).then(res => {
            return res.json()
        })
          .then(data => {
              console.log(data);
              if(data){
                  //set token to local storage for future verification.
                  localStorage.setItem('token', data.access);
                  fetch('https://polar-citadel-90746.herokuapp.com/api/users/details', {
                      method: 'GET',
                      headers: {
                          authorization: `Bearer ${data.access}`
                      }
                  }).then(res => {
                      return res.json()
                  }).then(data => {
                      console.log(data);
                      localStorage.setItem('id', data._id);
                      localStorage.setItem('isAdmin', data.isAdmin);
                      if(data.isAdmin === false){
                        window.location.replace('./profile.html');
                      }else{
                          window.location.replace('../index.html');
                      }
                      
                  });
              }else{
                  alert('Something went wrong, please check your credentials.');
              }
          })
    }

});